<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ include file="../Layout/taglib.jsp"%>

<form class="navbar-form navbar-left" action="/searchBooks.html"
	method="POST">
	<input name="value" type="text" class="form-control"
		placeholder="Search..."> <select name="criteria"
		class="form-control">
		<option>By Title</option>
		<option>By Author</option>
		<option>By Genre</option>
	</select>
</form>

<security:authorize access="hasRole('ROLE_ADMIN')">
	<div class="navbar-form navbar-right">
		<form action="/downloadReport.html">
			<input type="submit" name="reportType" value="pdf" class="btn btn-primary btn-active"/>
			<input type="submit" name="reportType" value="csv" class="btn btn-primary btn-active"/>
			<button type="button" class="btn btn-primary btn-success"
			data-toggle="modal" data-target="#myModal1">Add Book</button>
		</form>
	</div>
</security:authorize>

<table class="table table-bordered table-hover table-striped">
	<thead>
		<tr>
			<th>ISBN</th>
			<th>Name</th>
			<th>Author</th>
			<th>Genre</th>
			<th>Quantity</th>
			<th>Price</th>
			<th>Sell</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${books}" var="books" varStatus="i">
			<tr>
				<td>${books.getISBN()}</td>
				<td>${books.getTitle()}</td>
				<td>${books.getAuthor()}</td>
				<td>${books.getGenre().toString()}</td>
				<td>${books.getQuantity()}</td>
				<td>${books.getPrice()}</td>
				<td colspan="2">
				<form:form commandName="formResult"
						action="/CRUDBook.html" method="POST">
						<form:hidden path="isbn" value="${books.getISBN() }" />
						<form:input path="quantity" placeholder="0" />
						<input name="S" type="submit" value="S"
							class="btn btn-primary btn-warning" />
						<security:authorize access="hasRole('ROLE_ADMIN')">
							<input name="A" type="submit" value="A"
								class="btn btn-primary btn-success" />
							<input name="D" type="submit" value="D"
								class="btn btn-primary btn-danger" />
								<button type="button" class="btn btn-primary btn-info"
							data-toggle="modal" data-target="#myModal">U</button>
						</security:authorize>
					</form:form> <security:authorize access="hasRole('ROLE_ADMIN')">
						
					</security:authorize> 
					
	
					
					
					
					
		
<form:form commandName="newBook" action="/UpdateBook.html">
						<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
							aria-labelledby="myModalLabel">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal"
											aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
										<h4 class="modal-title" id="myModalLabel">Update Book</h4>
									</div>
									<div class="modal-body">
										<div class="form-group">
											<label for="ISBN" class="col-sm-2 control-label">ISBN:</label>
											<div class="col-sm-10">
												<form:input path="ISBN" cssClass="form-control"
													required="required" value="${books.getISBN() }" 
													readonly="true"/>
											</div>
										</div>
										<div class="form-group">
											<label for="author" class="col-sm-2 control-label">Author:</label>
											<div class="col-sm-10">
												<form:input path="author" cssClass="form-control"
													required="required" value="${books.getAuthor() }" />
											</div>
										</div>
										<div class="form-group">
											<label for="title" class="col-sm-2 control-label">Title:</label>
											<div class="col-sm-10">
												<form:input path="title" cssClass="form-control"
													required="required" value="${books.getTitle() }" />
											</div>
										</div>
										<div class="form-group">
											<label for="genre" class="col-sm-2 control-label">Genre:</label>
											<div class="col-sm-10">
												<form:select path="genre" cssClass="form-control">
													<form:option value="ACTION"
														selected="${books.getGenre() == 'ACTION' ?'selected' : '' }"></form:option>
													<form:option value="DETECTIVE"
														selected="${books.getGenre() == 'DETECTIVE' ?'selected' : '' }"></form:option>
													<form:option value="HORROR"
														selected="${books.getGenre() == 'HORROR' ?'selected' : '' }"></form:option>
													<form:option value="ROMANCE"
														selected="${books.getGenre() == 'ROMANCE' ?'selected' : '' }"></form:option>
													<form:option value="THRILLER"
														selected="${books.getGenre() == 'THRILLER' ?'selected' : '' }"></form:option>
												</form:select>
											</div>
										</div>
										<div class="form-group">
											<label for="quantity" class="col-sm-2 control-label">Quantity:</label>
											<div class="col-sm-10">
												<form:input path="quantity" cssClass="form-control"
													required="required" value="${books.getQuantity() }" />
											</div>
										</div>
										<div class="form-group">
											<label for="price" class="col-sm-2 control-label">Price:</label>
											<div class="col-sm-10">
												<form:input path="price" cssClass="form-control"
													required="required" value="${books.getPrice() }" />
											</div>
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default"
											data-dismiss="modal">Close</button>
										<input type="submit" name="U" class="btn btn-info"
											value="Update" />
									</div>
								</div>
							</div>
						</div>
					</form:form>
					
					
					
					
					
					
					
					
					
					
					
					
					</td>
			</tr>
		</c:forEach>
	</tbody>
</table>

				<form:form commandName="newBook">
	<div class="modal fade" id="myModal1" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">New Book</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="ISBN" class="col-sm-2 control-label">ISBN:</label>
						<div class="col-sm-10">
							<form:input path="ISBN" cssClass="form-control"
								required="required"/>
						</div>
					</div>
					<div class="form-group">
						<label for="author" class="col-sm-2 control-label">Author:</label>
						<div class="col-sm-10">
							<form:input path="author" cssClass="form-control"
								required="required"/>
						</div>
					</div>
					<div class="form-group">
						<label for="title" class="col-sm-2 control-label">Title:</label>
						<div class="col-sm-10">
							<form:input path="title" cssClass="form-control"
								required="required" />
						</div>
					</div>
					<div class="form-group">
						<label for="genre" class="col-sm-2 control-label">Genre:</label>
						<div class="col-sm-10">
							<form:select path="genre" cssClass="form-control">
								<form:option value="ACTION" ></form:option>
								<form:option value="DETECTIVE"
									selected="selected"></form:option>
								<form:option value="HORROR"
									></form:option>
								<form:option value="ROMANCE"
									></form:option>
								<form:option value="THRILLER"
									></form:option>
							</form:select>
						</div>
					</div>
					<div class="form-group">
						<label for="quantity" class="col-sm-2 control-label">Quantity:</label>
						<div class="col-sm-10">
							<form:input path="quantity" cssClass="form-control"
								required="required"  />
						</div>
					</div>
					<div class="form-group">
						<label for="price" class="col-sm-2 control-label">Price:</label>
						<div class="col-sm-10">
							<form:input path="price" cssClass="form-control"
								required="required" />
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<input type="submit" name="U" class="btn btn-info" value="Add Book" />
				</div>
			</div>
		</div>
	</div>
</form:form>
