<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ include file="../Layout/taglib.jsp" %>

<table class="table table-hover table-striped table-bordered">
  <thead>
  	<tr>
  		<th>First Name</th>
  		<th>Last Name</th>
  		<th>Username</th>
  		<th>E-mail</th>
  		<th>Admin</th>
  		<th></th>
  	</tr>
  </thead>
  <tbody>
  <c:forEach items="${employees}" var="employees">
  	<tr class="${employees.isAdmin()?'danger':'warning' }">
  		<td>${employees.getFirstName() }</td>
  		<td>${employees.getLastName() }</td>
  		<td>${employees.getUsername() }</td>
  		<td>${employees.getEmail() }</td>
  		<td>${employees.isAdmin() }</td>
  		<td>
  		
  		<form  action="/deleteEmployee/${employees.getUsername() }.html">
  			<input type="submit" class="btn btn-danger" value="-" style="${employees.isAdmin()?'display: none':'' }" />
  			</form>
  		</td>
  	<tr>
  	</c:forEach>
  </tbody>
</table>