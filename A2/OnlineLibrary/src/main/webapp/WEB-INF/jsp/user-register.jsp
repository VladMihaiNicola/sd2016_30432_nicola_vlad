<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ include file="../Layout/taglib.jsp"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>

<form:form commandName="user" cssClass="form-horizontal">

	<c:if test="${param.success eq true}">
		<div class="alert alert-success">
			Regisration Successfull!
		</div>
	</c:if>
	<c:if test="${param.success eq false }">
		<div class="alert alert-danger">
			Username Taken!!!
		</div>
	</c:if>

	<div class="form-group">
		<label for="username" class="col-sm-2 control-label">Username:</label>
		<div class="col-sm-10">
			<form:input path="username" cssClass="form-control" required="required"/>
		</div>
	</div>
	<div class="form-group">
		<label for="password" class="col-sm-2 control-label">Password:</label>
		<div class="col-sm-10">
			<form:password path="password" cssClass="form-control" required="required"/>
		</div>
	</div>
	<div class="form-group">
		<label for="firstName" class="col-sm-2 control-label">First
			Name:</label>
		<div class="col-sm-10">
			<form:input path="firstName" cssClass="form-control" required="required"/>
		</div>
	</div>
	<div class="form-group">
		<label for="lastName" class="col-sm-2 control-label">Last
			Name:</label>
		<div class="col-sm-10">
			<form:input path="lastName" cssClass="form-control" required="required"/>
		</div>
	</div>
	<div class="form-group">
		<label for="email" class="col-sm-2 control-label">E-mail:</label>
		<div class="col-sm-10">
			<form:input type="email" path="email" cssClass="form-control" required="required"/>
		</div>
	</div>
	<security:authorize access="hasRole('ROLE_ADMIN')">
		<div class="checkbox">
			<label for="admin"> <form:checkbox path="admin" /> Admin
			</label>
		</div>
	</security:authorize>
	<div class="form-group">
		<div class="col-sm-2">
			<input type="submit" value="Submit" class="btn btn-lg btn-primary" />
		</div>
	</div>
</form:form>