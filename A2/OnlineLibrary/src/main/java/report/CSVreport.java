package report;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import jaxb.Book;
import service.BookService;

public class CSVreport extends AbstractReport {

	public CSVreport()
	{
		super();
	}
	
	public ByteArrayOutputStream generateData(String path)
	{
		ArrayList<String> rows = new ArrayList<String>();
		rows.add("ISBN,Title,Author,Genre\n");
		
		BookService bs = new BookService();
		List<Book> books = bs.getOutOfStock();
		
		for(Book b : books)
		{
			rows.add(Long.toString(b.getISBN()) + "," + b.getTitle() + "," + b.getAuthor() + "," + b.getGenre().toString() + "\n");
		}
		
		FileOutputStream f = null;
		try {
			f = new FileOutputStream(path);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for(String s : rows)
		{
			try {
				f.write(s.getBytes());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			f.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		ByteArrayOutputStream baos = convertPDFToByteArrayOutputStream(path);
		return baos;
	}
}
