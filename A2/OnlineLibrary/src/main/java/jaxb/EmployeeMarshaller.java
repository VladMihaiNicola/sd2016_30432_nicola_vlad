package jaxb;

import java.io.File;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class EmployeeMarshaller {

	public EmployeeMarshaller()
	{

	}
	
	public int getRole(String username, String password)
	{
		Employees emp = unmarshallEmployees();
		for(Employee e : emp.getEmployees())
		{
			if(e.matches(username, password))
			{
				if(e.isAdmin())
					return 2;
				else
					return 1;
			}
		}
		return 0;
	}

	public Employees unmarshallEmployees()
	{
		File f = new File("src/main/java/jaxb/employees.xml");
		
		Employees emp = null;
		try {
			JAXBContext cont = JAXBContext.newInstance(Employees.class);
			Unmarshaller unmarsh = cont.createUnmarshaller();

			emp = (Employees)unmarsh.unmarshal(f);

		} catch (JAXBException exc) {
			emp = new Employees();
		}
		return emp;
	}
	
	public boolean marshallEmployee(Employee e)
	{
		File f = new File("src/main/java/jaxb/employees.xml");
		Employees emp = unmarshallEmployees();

		for(int i = 0; i < emp.getEmployees().size(); ++i)
		{
			if(emp.getEmployees().get(i).getUsername().compareTo(e.getUsername()) == 0)
				return false;
		}
		
		try {
			JAXBContext cont = JAXBContext.newInstance(Employees.class);

			Marshaller marsh = cont.createMarshaller();
			marsh.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			
			emp.addEmployee(e);
			marsh.marshal(emp, f);
		} catch (JAXBException e1) {
			e1.printStackTrace();
		}
		return true;
	}

	public List<Employee> printAll() {
		Employees emps = unmarshallEmployees();
		return emps.getEmployees();
	}

	public void marshallEmployees(Employees e)
	{
		File f = new File("src/main/java/jaxb/employees.xml");

		try {
			JAXBContext cont = JAXBContext.newInstance(Employees.class);

			Marshaller marsh = cont.createMarshaller();
			marsh.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			
			marsh.marshal(e, f);
		} catch (JAXBException e1) {
			e1.printStackTrace();
		}
	}
	
	public void deleteEmployee(String user) {
		Employees emps = unmarshallEmployees();
		for(int i = 0; i < emps.getEmployees().size(); ++i)
		{
			if(emps.getEmployees().get(i).getUsername().compareTo(user) == 0)
			{
				emps.getEmployees().remove(i);
				break;
			}
		}
		marshallEmployees(emps);
		
	}
}
