package jaxb;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class BooksUnmarshaller {
	
	public List<Book> booksList;
	
	public BooksUnmarshaller()
	{
		booksList = new ArrayList<Book>();
	}
	
	public void unMarshallBooks()
	{
		try {
			JAXBContext cont = JAXBContext.newInstance(Books.class);
			Unmarshaller unmarsh = cont.createUnmarshaller();
			
			File f = new File("src/main/java/jaxb/books.xml");
			Books books = (Books)unmarsh.unmarshal(f);
			booksList = books.getBooks();
		} catch (JAXBException e) {
			booksList = new ArrayList<Book>();
		}
	}
	
	public List<Book> printAll()
	{
		unMarshallBooks();
		return booksList;
	}
	
	public boolean sellBoooks(long isbn, int quantity)
	{
		List<Book> booksList = printAll();
		for(int i = 0; i < booksList.size(); ++i)
		{
			if(booksList.get(i).getISBN() == isbn)
			{
				if(booksList.get(i).getQuantity() >= quantity)
				{
					booksList.get(i).setQuantity(booksList.get(i).getQuantity() - quantity);
				}
				else
				{
					booksList.get(i).setQuantity(0);
				}
				marshallBooks(booksList);
				return true;
			}
		}
		return false;
	}

	private void marshallBooks(List<Book> booksList2) {
		try {
			JAXBContext cont = JAXBContext.newInstance(Books.class);
			Marshaller marsh = cont.createMarshaller();
			
			marsh.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			
			File f = new File("src/main/java/jaxb/books.xml");
			marsh.marshal(new Books(booksList2), f);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}

	public List<Book> searchBy(String string, String string2) {
		List<Book> books = printAll();
		List<Book> resultBooks = new ArrayList<Book>();
		
		switch(string2)
		{
		case "By Genre":
			for(Book b : books)
			{
				if(b.getGenre().toString().compareTo(string) == 0)
					resultBooks.add(b);
			}
			break;
		case "By Author":
			for(Book b : books)
			{
				if(b.getAuthor().toLowerCase().contains(string.toLowerCase()))
					resultBooks.add(b);
			}
			break;
		case "By Title":
			for(Book b : books)
			{
				if(b.getTitle().toLowerCase().contains(string.toLowerCase()))
					resultBooks.add(b);
			}
		}
		
		return resultBooks;
	}

	public void addBook(Book book) {
		List<Book> booksList = printAll();
		booksList.add(book);
		marshallBooks(booksList);
	}

	public void deleteBook(long isbn) {
		List<Book> booksList = printAll();
		for(int i = 0; i < booksList.size(); ++i)
		{
			if(booksList.get(i).getISBN() == isbn)
			{
				booksList.remove(booksList.get(i));
				break;
			}
		}
		marshallBooks(booksList);
	}
	

	public void updateBook(Book book) {
		List<Book> booksList = printAll();
		for(int i = 0; i < booksList.size(); ++i)
		{
			if(booksList.get(i).getISBN() == book.getISBN())
			{
				booksList.remove(booksList.get(i));
				booksList.add(i,book);
				break;
			}
		}
		marshallBooks(booksList);
	}

	public List<Book> getOutOfStock() {	
		List<Book> booksList = printAll();
		List<Book> result = new ArrayList<Book>();
		for(int i = 0; i < booksList.size(); ++i)
		{
			if(booksList.get(i).getQuantity() == 0)
			{
				result.add(booksList.get(i));
			}
		}
		return result;
	}
}
