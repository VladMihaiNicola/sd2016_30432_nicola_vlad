package jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="employee")
@XmlAccessorType(XmlAccessType.FIELD)
public class Employee {

	private String username;
	private String password;
	private boolean admin;
	private String firstName;
	private String lastName;
	private String email;
	
	public Employee(){}
	
	public Employee(String username, String password, boolean admin, String firstName, String lastName, String email) {
		this.username = username;
		this.password = password;
		this.admin = admin;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
	}
	
	

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public boolean isAdmin() {
		return admin;
	}
	public void setAdmin(boolean admin) {
		this.admin = admin;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public boolean matches(String username, String password)
	{
		if(this.username.compareTo(username) == 0 && this.password.compareTo(password) == 0)
		{
			return true;
		}
		return false;
	}
}
