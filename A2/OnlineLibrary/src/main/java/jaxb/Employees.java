package jaxb;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="employees")
@XmlAccessorType(XmlAccessType.FIELD)
public class Employees {

	@XmlElement(name="employee")
	private List<Employee> employeesList;
	
	public Employees()
	{
		employeesList = new ArrayList<Employee>();
	}
	
	public void addEmployee(Employee e)
	{
		employeesList.add(e);
	}
	
	public List<Employee> getEmployees()
	{
		return employeesList;
	}
}
