package service;

import java.util.List;

import jaxb.Employee;
import jaxb.EmployeeMarshaller;

public class EmployeeService {

	EmployeeMarshaller em;
	
	public EmployeeService()
	{
		em = new EmployeeMarshaller();
	}
	
	public boolean saveUser(Employee user)
	{
		return em.marshallEmployee(user);
	}

	public List<Employee> printAll() {
		return em.printAll();
	}

	public void deleteEmployee(String user) {
		em.deleteEmployee(user);
		
	}
}
