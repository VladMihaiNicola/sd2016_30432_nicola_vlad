package service;

import java.io.File;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jaxb.Book;
import jaxb.Books;
import jaxb.BooksUnmarshaller;

@Service
public class BookService {
	
	private BooksUnmarshaller bu;
	
	public BookService()
	{
		bu = new BooksUnmarshaller();
	}
	
	public List<Book> printAll()
	{
		return bu.printAll();
	}

	public boolean sellBooks(long isbn, int quantity) {
		if(quantity < 0)
			return false;
		return bu.sellBoooks(isbn,quantity);
	}

	public List<Book> searchBy(String string, String string2) {
		return bu.searchBy(string, string2);
	}

	public void addBook(Book book) {
		bu.addBook(book);
	}

	public boolean addBooks(long isbn, int quantity) {
		if(quantity < 0)
			return false;
		return bu.sellBoooks(isbn, quantity * -1);
	}

	public void deleteBook(long isbn) {
		bu.deleteBook(isbn);		
	}

	public void updateBook(Book book) {
		bu.updateBook(book);
	}

	public List<Book> getOutOfStock() {
		return bu.getOutOfStock();
	}
}
