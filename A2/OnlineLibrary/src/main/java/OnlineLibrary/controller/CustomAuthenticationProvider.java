package OnlineLibrary.controller;

import java.util.ArrayList;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import jaxb.EmployeeMarshaller;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider{

	@Override
	public Authentication authenticate(Authentication arg0) throws AuthenticationException {
		String name = arg0.getName();
		String password = arg0.getCredentials().toString();
		
		EmployeeMarshaller em = new EmployeeMarshaller();
		int role = em.getRole(name,password);
		ArrayList<GrantedAuthority> auth = new ArrayList<GrantedAuthority>();
		if(role != 0)
		{
			if(role == 2)
				auth.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
			else if(role == 1)
				auth.add(new SimpleGrantedAuthority("ROLE_USER"));
			
			return new UsernamePasswordAuthenticationToken(name, password, auth);
		}
		return null;
	}

	@Override
	public boolean supports(Class<?> arg0) {
		return arg0.equals(UsernamePasswordAuthenticationToken.class);
	}


}
