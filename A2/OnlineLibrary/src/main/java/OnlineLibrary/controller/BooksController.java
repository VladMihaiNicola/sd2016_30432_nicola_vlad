package OnlineLibrary.controller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import jaxb.Book;
import report.AbstractReport;
import report.CSVreport;
import report.PDFreport;
import service.BookService;

@Controller
public class BooksController {

	public BookService bs;
	
	@ModelAttribute("formResult")
	public FormResult construct()
	{
		return new FormResult();
	}
	
	@ModelAttribute("newBook")
	public Book constructBook()
	{
		return new Book();
	}
	
	@RequestMapping(value="/searchBooks", method=RequestMethod.POST)
	public String searchBooks(@RequestParam Map<String,String> params, Model model)
	{
		bs = new BookService();
		List<Book> booksList = bs.searchBy(params.get("value"),params.get("criteria"));
		model.addAttribute("books", booksList);
		return "books";
	}
	
	@RequestMapping(value="/CRUDBook", method=RequestMethod.POST, params="S")
	public String sellBook(@ModelAttribute("formResult") FormResult books)
	{
		bs = new BookService();
		bs.sellBooks(books.getIsbn(), books.getQuantity());		
		return "redirect:/books.html?success=true";
	}
	
	@RequestMapping(value="/CRUDBook", method=RequestMethod.POST, params="A")
	public String addBook(@ModelAttribute("formResult") FormResult books)
	{
		bs = new BookService();
		bs.addBooks(books.getIsbn(), books.getQuantity());
		return "redirect:/books.html?success=true";
	}
	
	@RequestMapping(value="/CRUDBook", method=RequestMethod.POST, params="D")
	public String deleteBook(@ModelAttribute("formResult") FormResult books)
	{
		bs = new BookService();
		bs.deleteBook(books.getIsbn());
		return "redirect:/books.html?success=true";
	}
	
	@RequestMapping(value="/UpdateBook", method=RequestMethod.POST)
	public String deleteBook(@ModelAttribute("newBook") Book book)
	{
		bs = new BookService();
		bs.updateBook(book);
		return "redirect:/books.html?success=true";
	}
	
	@RequestMapping("/books")
	public String books(Model model)
	{
		bs = new BookService();
		model.addAttribute("books", bs.printAll());
		return "books";
	}
	
	@RequestMapping(value="/books", method=RequestMethod.POST)
	public String addNewBook(@ModelAttribute("newBook") Book book)
	{
		bs = new BookService();
		bs.addBook(book);
		return "redirect:/books.html";
	}
	
	@RequestMapping(value = "/downloadReport")
	public void downloadReport(HttpServletRequest request, HttpServletResponse response, @RequestParam("reportType") String rt) throws IOException {
		final ServletContext servletContext = request.getSession().getServletContext();
	    final File tempDirectory = (File) servletContext.getAttribute("javax.servlet.context.tempdir");
	    final String temperotyFilePath = tempDirectory.getAbsolutePath();
	    
	    AbstractReport report = null;
	    String fileName = "";
	    System.out.println(rt);
	    if(rt.compareTo("pdf") == 0)
	    {
	    	report = new PDFreport();
	    	fileName = "PDFReport.pdf";
	    	response.setContentType("application/pdf");
	    }
	    else if(rt.compareTo("csv") == 0)
	    {
	    	report = new CSVreport();
	    	fileName = "CSVReport.csv";
	    	response.setContentType("application/octet-stream");
	    }
	    
	    response.setHeader("Content-disposition", "attachment; filename="+ fileName);
	    
	    ByteArrayOutputStream baos = report.generateData(temperotyFilePath + "\\" + fileName);
	    OutputStream os = response.getOutputStream();
        baos.writeTo(os);
        os.flush();
	}
	
	private class FormResult
	{
		private int quantity;
		private long isbn;

		public FormResult() {
		}
		

		@SuppressWarnings("unused")
		public void setQuantity(int quantity) {
			this.quantity = quantity;
		}


		public int getQuantity() {
			return quantity;
		}

		public long getIsbn() {
			return isbn;
		}
		
		@SuppressWarnings("unused")
		public void setIsbn(long isbn) {
			this.isbn = isbn;
		}
		
	}
}
