package OnlineLibrary.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jaxb.Employee;
import service.BookService;
import service.EmployeeService;

@Controller
public class EmployeeController {

	private EmployeeService es;
	
	@ModelAttribute("user")
	public Employee construct()
	{
		return new Employee();
	}
	
	@RequestMapping(value="/register", method=RequestMethod.POST)
	public String doRegister(@ModelAttribute("user") Employee user)
	{		
		es = new EmployeeService();
		if(es.saveUser(user))
			return "redirect:/register.html?success=true";
		else
			return "redirect:/register.html?success=false";
	}
	
	@RequestMapping("/register")
	public String showRegister()
	{
		return "user-register";
	}
	
	@RequestMapping("/employees")
	public String showEmployees(Model model)
	{
		es = new EmployeeService();
		model.addAttribute("employees", es.printAll());
		return "employees";
	}
	
	@RequestMapping("/deleteEmployee/{usrname}")
	public String deleteEmployee(@PathVariable("usrname") String user)
	{
		es = new EmployeeService();
		es.deleteEmployee(user);
		return "redirect:/employees.html";
	}
}
