package HospitalApplication.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import HospitalApplication.entity.User;
import HospitalApplication.service.UserService;

@Controller
public class UserController {

	@Autowired
	private UserService userService;
	
	@ModelAttribute("user")
	public User construct()
	{
		return new User();
	}
	
	@RequestMapping(value="/user-register", method=RequestMethod.POST)
	public String userRegister(@ModelAttribute("user") User user, @RequestParam("confPass") String confPass)
	{
		if(user.getPassword().compareTo(confPass) != 0)
		{
			return "error";
		}
		userService.save(user);
		return "user-register";
	}
	
	@RequestMapping("/users")
	public String users(Model model)
	{
		model.addAttribute("users", userService.findAll());
		return "users";
	}
	
	@RequestMapping("/user-register")
	public String userRegister()
	{
		return "user-register";
	}
}
