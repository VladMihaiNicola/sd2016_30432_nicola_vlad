package HospitalApplication.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import HospitalApplication.entity.Consultation;

public interface ConsultationRepository extends JpaRepository<Consultation, Integer>{

}
