package dataSourceLayer;
import java.sql.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import presentationLayer.WindowCreator;


public class DBGateway {

	private SessionFactory sf;
	
	public DBGateway()
	{
		Configuration cfg = new Configuration();
		cfg.configure("hibernate.cfg.xml");
		sf = cfg.buildSessionFactory();
	}
	
	public void addClient(Client c)
	{
		Session s = sf.openSession();
		Transaction tx = s.beginTransaction();
		s.save(c);
		s.flush();
		tx.commit();
		s.close();
	}
	
	public void updateClient(Client c)
	{
		Session s = sf.openSession();
		Transaction tx = s.beginTransaction();
		s.update(c);
		s.flush();
		tx.commit();
		s.close();
	}
	
	public Client getClient(long cnp)
	{
		Session s = sf.openSession();
		Transaction tx = s.beginTransaction();
		Client c = (Client)s.load(Client.class, cnp);
		s.flush();
		tx.commit();
		s.close();
		return c;
	}
	
	@SuppressWarnings("unchecked")
	public List<Employee> getEmployee(String username, String password)
	{
		Session s = sf.openSession();
		Transaction tx = s.beginTransaction();
		
		String hql = "FROM Employee WHERE username = :user AND password = :pass";
		Query q = s.createQuery(hql);
		q.setParameter("user", username);
		q.setParameter("pass", password);
		List<Employee> lst = (List<Employee>)q.list();
		s.flush(); tx.commit(); s.close();
		return lst;
	}
	
	@SuppressWarnings("unchecked")
	public List<Client> getClients(String name)
	{
		Session s = sf.openSession();
		Transaction tx = s.beginTransaction();
		String hql = "FROM Client WHERE first_name LIKE :name OR last_name LIKE :name";
		Query q = s.createQuery(hql);
		q.setParameter("name", "%" + name + "%");
		List<Client> clients = (List<Client>)q.list();
		s.flush(); tx.commit(); s.close();
		return clients;
	}
	
	public void addAccount(Client c, String type, long balance)
	{
		long ownerCnp = c.getCnp();
		Date creationDate = new Date(System.currentTimeMillis());
		
		Account acc = new Account();
		acc.setBalance(balance);
		acc.setCreationDate(creationDate);
		acc.setOwnerCnp(ownerCnp);
		acc.setType(type);
		
		Session s = sf.openSession();
		Transaction tx = s.beginTransaction();
		s.save(acc);
		s.flush(); tx.commit(); s.close();
	}
	
	@SuppressWarnings("unchecked")
	public List<Account> getAccounts(Client c)
	{
		long ownerCnp = c.getCnp();
		
		Session s = sf.openSession();
		Transaction tx = s.beginTransaction();
		
		String hql = "FROM Account WHERE owner_cnp = :cnp";
		Query q = s.createQuery(hql);
		q.setParameter("cnp", ownerCnp);
		List<Account> accounts = (List<Account>)q.list();
		s.flush(); tx.commit(); s.close();
		return accounts;
	}
	
	public void deleteAccount(Account acc)
	{
		Session s = sf.openSession();
		Transaction tx = s.beginTransaction();
		
		s.delete(acc);
		
		s.flush(); tx.commit(); s.close();
	}
	
	public void updateAccount(Account acc)
	{
		Session s = sf.openSession();
		Transaction tx = s.beginTransaction();
		
		s.update(acc);
		
		s.flush(); tx.commit(); s.close();
	}
	
	public void transferMoney()
	{
		
	}
	
	public void payTaxes()
	{
		
	}
	
	@SuppressWarnings("unchecked")
	public List<Employee> getEmployees(String name)
	{
		Session s = sf.openSession();
		Transaction tx = s.beginTransaction();
		
		String hql = "FROM Employee WHERE first_name LIKE :firstName OR last_name LIKE :lastName";
		Query q = s.createQuery(hql);
		q.setParameter("firstName", "%" + name + "%");
		q.setParameter("lastName", "%" + name + "%");
		List<Employee> employees = (List<Employee>)q.list();
		s.flush(); tx.commit(); s.close();
		
		return employees;
	}
	
	public void addEmployee(Employee e)
	{
		Session s = sf.openSession();
		Transaction tx = s.beginTransaction();
		
		s.save(e);
		
		s.flush(); tx.commit(); s.close();
	}
	
	public void updateEmployee(Employee e)
	{
		Session s = sf.openSession();
		Transaction tx = s.beginTransaction();
		
		s.update(e);
		
		s.flush(); tx.commit(); s.close();
	}
	
	public void deleteEmployee(Employee e)
	{
		Session s = sf.openSession();
		Transaction tx = s.beginTransaction();
		
		s.delete(e);
		
		s.flush(); tx.commit(); s.close();
	}
	
	@SuppressWarnings("unchecked")
	public List<Action> getReport(Employee emp)
	{
		Session s = sf.openSession();
		Transaction tx = s.beginTransaction();
		
		String hql = "FROM Action WHERE employee_id = :id";
		Query q = s.createQuery(hql);
		q.setParameter("id", emp.getId());
		
		List<Action> list = q.list();
		
		s.flush(); tx.commit(); s.close();
		return list;
	}
	
	public void markAction(Action ac)
	{
		Session s = sf.openSession();
		Transaction tx = s.beginTransaction();
		
		s.save(ac);
		
		s.flush(); tx.commit(); s.close();
	}
	
	public void transferMoney(Account from, Account to, long sum)
	{
		Session s = sf.openSession();
		Transaction tx = s.beginTransaction();
		
		from = (Account)s.load(Account.class, from.getId());
		from.setBalance(from.getBalance() - sum);
		s.update(from);
		
		to = (Account)s.load(Account.class, to.getId());
		to.setBalance(to.getBalance() + sum);
		s.update(to);
		
		s.flush(); tx.commit(); s.close();
	}
	
	public static void main(String[] args)
	{
		new WindowCreator();
	}
}
