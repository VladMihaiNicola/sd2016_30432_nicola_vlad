package dataSourceLayer;
import java.io.Serializable;


public class Client implements Serializable{
	private long cnp;
	private String idSeries;
	private int idNumber;
	private String address;
	private long phoneNumber;
	private String firstName;
	private String lastName;
	
	public Client()
	{
		
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public long getCnp() {
		return cnp;
	}
	public void setCnp(long cnp2) {
		this.cnp = cnp2;
	}
	public String getIdSeries() {
		return idSeries;
	}
	public void setIdSeries(String idSeries) {
		this.idSeries = idSeries;
	}
	public int getIdNumber() {
		return idNumber;
	}
	public void setIdNumber(int idNumber) {
		this.idNumber = idNumber;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public long getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(long phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	private static final long serialVersionUID = 1L;
}
