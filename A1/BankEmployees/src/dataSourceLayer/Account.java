package dataSourceLayer;
import java.io.Serializable;
import java.util.Date;

//@Entity
//@Table(name="accounts")
public class Account implements Serializable{

	//@Id
	//@GeneratedValue
	//@Column(name="account_id")
	private int id;
	
	//@Column(name="type")
	private String type;
	
	//@Column(name="balance")
	private long balance;
	
	//@Column(name="creation_date")
	private Date creationDate;
	
	//@Column(name="owner_cnp")
	private long ownerCnp;
	
	public Account()
	{
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public long getBalance() {
		return balance;
	}

	public void setBalance(long balance) {
		this.balance = balance;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public long getOwnerCnp() {
		return ownerCnp;
	}

	public void setOwnerCnp(long ownerCnp) {
		this.ownerCnp = ownerCnp;
	}
	
	private static final long serialVersionUID = 1L;
}
