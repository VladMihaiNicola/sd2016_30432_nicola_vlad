package domainLogicLayer;
import java.util.Date;

import dataSourceLayer.Action;
import dataSourceLayer.DBGateway;
import dataSourceLayer.Employee;


public abstract class User {

	protected Employee m_me;
	protected DBGateway m_dbg;
	
	public User(Employee me, DBGateway dbg)
	{
		m_me = me;
		m_dbg = dbg;
	}
	
	public Employee getEmployee()
	{
		return m_me;
	}
	
	public void markAction(String code, String description)
	{
		Action ac = new Action();
		
		ac.setCode(code);
		ac.setDate(new Date(System.currentTimeMillis()));
		ac.setDescription(description);
		ac.setEmployeeId(m_me.getId());
		
		m_dbg.markAction(ac);
	}
}
