package domainLogicLayer;
import java.util.Date;
import java.util.List;

import dataSourceLayer.Action;
import dataSourceLayer.DBGateway;
import dataSourceLayer.Employee;


public class AdminUser extends User{

	public AdminUser(Employee me, DBGateway dbg)
	{
		super(me,dbg);
	}
	
	public List<Employee> getEmployees(String name)
	{
		return m_dbg.getEmployees(name);
	}
	
	public void addEmployee(String username, String password, String firstName, String lastName, String department)
	{
		Employee emp = new Employee();
		emp.setAdmin(false);
		emp.setDepartment(department);
		emp.setFirstName(firstName);
		emp.setHiringDate(new Date(System.currentTimeMillis()));
		emp.setLastName(lastName);
		emp.setPassword(password);
		emp.setUsername(username);
		
		m_dbg.addEmployee(emp);
	}
	
	public void updateEmployee(int id, String username, String password, String firstName, String lastName, String department, Date hiringDate, boolean admin)
	{
		Employee emp = new Employee();
		emp.setId(id);
		emp.setAdmin(admin);
		emp.setDepartment(department);
		emp.setFirstName(firstName);
		emp.setHiringDate(hiringDate);
		emp.setLastName(lastName);
		emp.setPassword(password);
		emp.setUsername(username);
		
		m_dbg.updateEmployee(emp);
	}
	
	public void deleteEmployee(Employee emp)
	{
		m_dbg.deleteEmployee(emp);
	}
	
	public List<Action> getReport(Employee emp)
	{
		return m_dbg.getReport(emp);
	}
}
