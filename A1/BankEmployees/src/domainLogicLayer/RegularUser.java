package domainLogicLayer;
import java.util.Date;
import java.util.List;

import dataSourceLayer.Account;
import dataSourceLayer.Client;
import dataSourceLayer.DBGateway;
import dataSourceLayer.Employee;


public class RegularUser extends User{
	
	public RegularUser(Employee me, DBGateway dbg)
	{
		super(me,dbg);
	}
	
	public void addClient(long cnp, String address, int idNumber, String idSeries, long phoneNumber, String firstName, String lastName)
	{
		Client c = new Client();
		
		c.setCnp(cnp);
		c.setAddress(address);
		c.setIdNumber(idNumber);
		c.setIdSeries(idSeries);
		c.setPhoneNumber(phoneNumber);
		c.setFirstName(firstName);
		c.setLastName(lastName);
		
		m_dbg.addClient(c);
	}
	
	public void updateClient(long cnp, String address, int idNumber, String idSeries, long phoneNumber, String firstName, String lastName)
	{
		Client c = new Client();
		
		c.setCnp(cnp);
		c.setAddress(address);
		c.setIdNumber(idNumber);
		c.setIdSeries(idSeries);
		c.setPhoneNumber(phoneNumber);
		c.setFirstName(firstName);
		c.setLastName(lastName);
		
		m_dbg.updateClient(c);
	}
	
	public List<Client> getClients(String name)
	{
		return m_dbg.getClients(name);
	}
	
	public List<Account> getAccounts(Client c)
	{
		return m_dbg.getAccounts(c);
	}
	
	public void deleteAccount(Account acc)
	{
		m_dbg.deleteAccount(acc);
	}
	
	public void updateAccount(int id, String type, long balance, long ownerCnp)
	{
		Account acc = new Account();
		acc.setId(id);
		acc.setType(type);
		acc.setBalance(balance);
		acc.setCreationDate(new Date(System.currentTimeMillis()));
		acc.setOwnerCnp(ownerCnp);
		
		m_dbg.updateAccount(acc);
	}
	
	public void addAccount(Client c, String type, long balance)
	{
		m_dbg.addAccount(c, type, balance);
	}
	
	public void transferCash(Account fromAcc, int toAccId, long sum)
	{
		Account toAcc = new Account();
		toAcc.setId(toAccId);
		
		m_dbg.transferMoney(fromAcc, toAcc, sum);
	}
}
