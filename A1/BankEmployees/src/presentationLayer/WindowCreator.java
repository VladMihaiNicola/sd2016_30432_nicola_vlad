package presentationLayer;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import dataSourceLayer.Account;
import dataSourceLayer.Action;
import dataSourceLayer.Client;
import dataSourceLayer.DBGateway;
import dataSourceLayer.Employee;
import domainLogicLayer.AdminUser;
import domainLogicLayer.RegularUser;
import domainLogicLayer.User;


public class WindowCreator {
	private JFrame m_frame;
	private JPanel m_panel;
	private User user;
	
	public WindowCreator()
	{
		m_frame = new JFrame();
		m_frame.setTitle("BankApplication");
		m_frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		m_frame.setSize(600, 300);
		m_frame.setLayout(new BorderLayout());
		
		m_panel = new JPanel();
		m_frame.getContentPane().add(m_panel, BorderLayout.NORTH);
		
		m_frame.setVisible(true);
		goToCredentials();
	}
	
	private void goToCredentials()
	{
		m_frame.getContentPane().removeAll();
		
		m_panel = new JPanel();
		m_panel.setLayout(new GridLayout(0,1));
		
		final JTextField username  = new JTextField("Username");
		final JPasswordField password  = new JPasswordField("Password");
		final JButton    logIn     = new JButton("Log in");
		
		logIn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				if(validateInput(username.getText(), new String(password.getPassword())))
				{
					if(user instanceof RegularUser) goToMainMenu_Regular();
					else if(user instanceof AdminUser)   goToMainMenu_Admin();
				}
				else
				{
					System.exit(0);
				}
			}
		});
		
		m_panel.add(username);
		m_panel.add(password);
		m_panel.add(logIn);
		
		m_frame.add(m_panel, BorderLayout.NORTH);
		m_frame.repaint(); m_frame.revalidate();
	}
	
	private void goToMainMenu_Admin()
	{
		m_frame.getContentPane().removeAll();
		
		m_panel = new JPanel();
		m_panel.setLayout(new FlowLayout());
		
		final JTextField searchField = new JTextField(30);
		
		final JButton addEmployee = new JButton("Add Employee");
		addEmployee.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {				
				final JDialog dialog = new JDialog();
				dialog.setSize(300, 250);
				dialog.setLocationRelativeTo(null);
				dialog.setResizable(false);
				dialog.setTitle("Add Employee");
				dialog.setModal(true);
				
				final JTextField usernameField = new JTextField(30);
				final JPasswordField passwordField = new JPasswordField(30);
				final JTextField firstNameField = new JTextField(30);
				final JTextField lastNameField = new JTextField(30);
				final JTextField departmentField = new JTextField(30);
				
				JButton done = new JButton("Done");
				done.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent e) {
						String username = usernameField.getText();
						String password = new String(passwordField.getPassword());
						String firstName = firstNameField.getText();
						String lastName = lastNameField.getText();
						String department = departmentField.getText();
						
						((AdminUser) user).addEmployee(username, password, firstName, lastName, department);
						user.markAction("Employee add", "Addition of " + firstName + " " + lastName);
						
						dialog.dispose();
					}
				});
				
				dialog.setLayout(new GridLayout(0,2));
				dialog.add(new JLabel("Username: ")); dialog.add(usernameField);
				dialog.add(new JLabel("Password: ")); dialog.add(passwordField);
				dialog.add(new JLabel("First Name: ")); dialog.add(firstNameField);
				dialog.add(new JLabel("Last Name:")); dialog.add(lastNameField);
				dialog.add(new JLabel("Department: ")); dialog.add(departmentField);
				dialog.add(done);
				
				dialog.setVisible(true);
				}
		});
		
		JButton back = new JButton("Back");
		back.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				goToMainMenu_Admin();
			}
		});
		
		final JButton searchEmployee = new JButton("Search");
		searchEmployee.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				final List<Employee> list = ((AdminUser) user).getEmployees(searchField.getText());
				m_frame.getContentPane().removeAll();
				m_frame.add(m_panel, BorderLayout.NORTH);
				
				String columnData[][] = new String[list.size()][6];
				for(int i = 0; i < list.size(); ++i)
				{
					columnData[i][0] = Integer.toString(list.get(i).getId());
					columnData[i][1] = list.get(i).getFirstName();
					columnData[i][2] = list.get(i).getLastName();
					columnData[i][3] = list.get(i).getUsername();
					columnData[i][4] = list.get(i).getDepartment();
					columnData[i][5] = list.get(i).getHiringDate().toString();
				}
				
				String columnNames[] = new String[]{"Employee ID", "First Name", "Last Name", "Username", "Department", "Hiring Date"};
				final JTable table = new JTable(columnData, columnNames);
				JScrollPane scroll = new JScrollPane(table);
				
				final JButton updateEmployee = new JButton("Update");
				updateEmployee.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent e) {
						int i = table.getSelectedRow();
						if(i != -1)
						{
							final Employee emp = list.get(i);
							
							final JDialog dialog = new JDialog();
							dialog.setSize(300, 250);
							dialog.setLocationRelativeTo(null);
							dialog.setResizable(false);
							dialog.setTitle("Update Client");
							dialog.setModal(true);
							
							final JTextField idField = new JTextField(Integer.toString(emp.getId()));
							final JTextField usernameField = new JTextField(emp.getUsername());
							final JTextField firstNameField = new JTextField(emp.getFirstName());
							final JTextField lastNameField = new JTextField(emp.getLastName());
							final JTextField departmentField = new JTextField(emp.getDepartment());
							final JTextField hiringDateField = new JTextField(emp.getHiringDate().toString());
							idField.setEditable(false);
							hiringDateField.setEditable(false);
							
							JButton done = new JButton("Done");
							done.addActionListener(new ActionListener(){
								public void actionPerformed(ActionEvent e) {
									int id = Integer.parseInt(idField.getText());
									String username = usernameField.getText();
									String firstName = firstNameField.getText();
									String lastName = lastNameField.getText();
									String department = departmentField.getText();
									Date hiringDate = emp.getHiringDate();
									
									((AdminUser) user).updateEmployee(id, username, emp.getPassword(), firstName, lastName, department, hiringDate, emp.isAdmin());
									user.markAction("Employee update", "Update for " + firstName + " " + lastName);
									
									dialog.dispose();
								}
							});
							
							dialog.setLayout(new GridLayout(0,2));
							dialog.add(new JLabel("ID: ")); dialog.add(idField);
							dialog.add(new JLabel("Username: ")); dialog.add(usernameField);
							dialog.add(new JLabel("First Name: ")); dialog.add(firstNameField);
							dialog.add(new JLabel("Last Name:")); dialog.add(lastNameField);
							dialog.add(new JLabel("Department: ")); dialog.add(departmentField);
							dialog.add(new JLabel("Hiring Date")); dialog.add(hiringDateField);
							dialog.add(done);
							
							dialog.setVisible(true);
						}
					}
				});
				
				JButton deleteEmployee = new JButton("Delete Employee");
				deleteEmployee.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent e) {
						int i = table.getSelectedRow();
						if(i != -1)
						{
							Employee emp = list.get(i);
							((AdminUser) user).deleteEmployee(emp);
							user.markAction("Employee delete", "Delete of " + emp.getFirstName() + " " + emp.getLastName());
						}
					}
				});
				
				JButton getReport = new JButton("Get Report");
				getReport.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent arg0) {
						int i = table.getSelectedRow();
						if(i != -1)
						{
							final Employee emp = list.get(i);
							
							JDialog dialog = new JDialog();
							dialog.setSize(700, 600);
							dialog.setLocationRelativeTo(null);
							dialog.setResizable(false);
							dialog.setTitle("Activity Report");
							dialog.setModal(true);
							
							List<Action> actions = ((AdminUser) user).getReport(emp);
							
							String columns[][] = new String[actions.size()][5];
							for(int j = 0; j < actions.size(); ++j)
							{
								columns[j][0] = Integer.toString(actions.get(j).getId());
								columns[j][1] = Integer.toString(actions.get(j).getEmployeeId());
								columns[j][2] = actions.get(j).getCode();
								columns[j][3] = actions.get(j).getDate().toString();
								columns[j][4] = actions.get(j).getDescription();
							}
							String rows[] = new String[]{"Action ID", "Employee ID", "Code", "Date", "Description"};
							JTable table = new JTable(columns, rows);
							JScrollPane scrollpane = new JScrollPane(table);
							
							dialog.setLayout(new BorderLayout());
							dialog.add(scrollpane, BorderLayout.CENTER);
							dialog.setVisible(true);
						}
					}
				});
				
				JPanel result = new JPanel();
				JPanel buttonsPanel = new JPanel();
				buttonsPanel.setLayout(new FlowLayout());
				result.setLayout(new BorderLayout());
				result.add(scroll, BorderLayout.CENTER);
				buttonsPanel.add(getReport);
				buttonsPanel.add(updateEmployee);
				buttonsPanel.add(deleteEmployee);
				result.add(buttonsPanel, BorderLayout.SOUTH);

				m_frame.add(result, BorderLayout.CENTER);
				m_frame.repaint(); m_frame.revalidate();
			}
		});
		
		m_panel.add(searchField);
		m_panel.add(searchEmployee);
		m_panel.add(addEmployee);
		m_panel.add(back);
		m_frame.add(m_panel, BorderLayout.NORTH);
		m_frame.repaint(); m_frame.revalidate();
	}
	
	private void goToMainMenu_Regular()
	{
		m_frame.getContentPane().removeAll();
		
		m_panel = new JPanel();
		m_panel.setLayout(new FlowLayout());
		
		JButton viewClients = new JButton("View Clients");
		viewClients.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				goToViewClientsPage();	
			}
		});
		
		JButton addClients = new JButton("Add Clients");
		addClients.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				final JDialog dialog = new JDialog();
				dialog.setSize(300, 250);
				dialog.setLocationRelativeTo(null);
				dialog.setResizable(false);
				dialog.setTitle("Update Client");
				dialog.setModal(true);
				
				final JTextField cnpField = new JTextField(30);
				final JTextField idSeriesField = new JTextField(30);
				final JTextField idNumberField = new JTextField(30);
				final JTextField addressField = new JTextField(30);
				final JTextField phoneNumberField = new JTextField(30);
				final JTextField firstNameField = new JTextField(30);
				final JTextField lastNameField = new JTextField(30);
				
				JButton done = new JButton("Done");
				done.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent e) {
						long cnp = Long.parseLong(cnpField.getText());
						int idNumber = Integer.parseInt(idNumberField.getText());
						String idSeries = idSeriesField.getText();
						long phoneNumber = Long.parseLong(phoneNumberField.getText());
						String firstName = firstNameField.getText();
						String lastName = lastNameField.getText();
						String address = addressField.getText();
						
						((RegularUser) user).addClient(cnp, address, idNumber, idSeries, phoneNumber, firstName, lastName);
						user.markAction("Client add", "Addition of " + firstName + " " + lastName);
						dialog.dispose();
					}
				});
				
				dialog.setLayout(new GridLayout(0,2));
				dialog.add(new JLabel("CNP:")); dialog.add(cnpField);
				dialog.add(new JLabel("ID Number:")); dialog.add(idNumberField);
				dialog.add(new JLabel("ID Series:")); dialog.add(idSeriesField);
				dialog.add(new JLabel("First Name:")); dialog.add(firstNameField);
				dialog.add(new JLabel("Last Name:")); dialog.add(lastNameField);
				dialog.add(new JLabel("Address:")); dialog.add(addressField);
				dialog.add(new JLabel("Phone Number:")); dialog.add(phoneNumberField);
				dialog.add(done);
				
				dialog.setVisible(true);
			}
		});
		
		JButton back = new JButton("Back");
		back.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				goToCredentials();
			}
		});
		
		m_panel.add(addClients);
		m_panel.add(viewClients);
		m_panel.add(back);
		m_frame.add(m_panel, BorderLayout.NORTH);
		m_frame.repaint(); m_frame.revalidate();
	}
	
	private boolean validateInput(String username, String password)
	{
		DBGateway dbg = new DBGateway();
		List<Employee> lst = dbg.getEmployee(username, password);
		if(lst.size() == 1)
		{
			Employee emp = lst.get(0);
			
			if(emp.isAdmin())
			{
				user = new AdminUser(emp, dbg);
			}
			else
			{
				user = new RegularUser(emp, dbg);
			}
			
			return true;
		}
		return false;
	}
	
	private void goToViewClientsPage()
	{
		m_frame.getContentPane().removeAll();
		
		m_panel = new JPanel();
		m_panel.setLayout(new FlowLayout());
		
		final JTextField searchField = new JTextField(30);
		final JButton searchButton = new JButton("Search");
		searchButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				final List<Client> list = ((RegularUser) user).getClients(searchField.getText());
				m_frame.getContentPane().removeAll();
				m_frame.add(m_panel, BorderLayout.NORTH);
				
				String columnData[][] = new String[list.size()][7];
				for(int i = 0; i < list.size(); ++i)
				{
					columnData[i][0] = list.get(i).getFirstName();
					columnData[i][1] = list.get(i).getLastName();
					columnData[i][2] = Long.toString(list.get(i).getCnp());
					columnData[i][3] = list.get(i).getIdSeries();
					columnData[i][4] = Integer.toString(list.get(i).getIdNumber());
					columnData[i][5] = list.get(i).getAddress();
					columnData[i][6] = Long.toString(list.get(i).getPhoneNumber());
				}
				
				String columnNames[] = new String[]{"First Name", "Last Name", "CNP", "ID Series", "ID Number", "Address", "Phone"};
				final JTable table = new JTable(columnData, columnNames);
				JScrollPane scroll = new JScrollPane(table);
				
				final JButton update = new JButton("Update");
				update.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent e) {
						int i = table.getSelectedRow();
						if(i != -1)
						{
							Client c = list.get(i);
							
							final JDialog dialog = new JDialog();
							dialog.setSize(300, 250);
							dialog.setLocationRelativeTo(null);
							dialog.setResizable(false);
							dialog.setTitle("Update Client");
							dialog.setModal(true);
							
							final JTextField cnpField = new JTextField(Long.toString(c.getCnp()));
							final JTextField idSeriesField = new JTextField(c.getIdSeries());
							final JTextField idNumberField = new JTextField(Integer.toString(c.getIdNumber()));
							final JTextField addressField = new JTextField(c.getAddress());
							final JTextField phoneNumberField = new JTextField(Long.toString(c.getPhoneNumber()));
							final JTextField firstNameField = new JTextField(c.getFirstName());
							final JTextField lastNameField = new JTextField(c.getLastName());
							cnpField.setEditable(false);
							
							JButton done = new JButton("Done");
							done.addActionListener(new ActionListener(){
								public void actionPerformed(ActionEvent e) {
									long cnp = Long.parseLong(cnpField.getText());
									int idNumber = Integer.parseInt(idNumberField.getText());
									String idSeries = idSeriesField.getText();
									long phoneNumber = Long.parseLong(phoneNumberField.getText());
									String firstName = firstNameField.getText();
									String lastName = lastNameField.getText();
									String address = addressField.getText();
									
									((RegularUser) user).updateClient(cnp, address, idNumber, idSeries, phoneNumber, firstName, lastName);
									user.markAction("Client update", "Update of " + firstName + " " + lastName);
									
									dialog.dispose();
								}
							});
							
							dialog.setLayout(new GridLayout(0,2));
							dialog.add(new JLabel("CNP:")); dialog.add(cnpField);
							dialog.add(new JLabel("ID Number:")); dialog.add(idNumberField);
							dialog.add(new JLabel("ID Series:")); dialog.add(idSeriesField);
							dialog.add(new JLabel("First Name:")); dialog.add(firstNameField);
							dialog.add(new JLabel("Last Name:")); dialog.add(lastNameField);
							dialog.add(new JLabel("Address:")); dialog.add(addressField);
							dialog.add(new JLabel("Phone Number:")); dialog.add(phoneNumberField);
							dialog.add(done);
							
							dialog.setVisible(true);
						}
					}
				});
				
				final JButton checkAccounts = new JButton("Check Accounts");
				checkAccounts.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent arg0) {
						int i = table.getSelectedRow();
						if(i != -1)
						{
							Client c = list.get(i);
							goToCheckAccounts(c);	
						}
					}
				});
				
				JPanel result = new JPanel();
				JPanel buttonsPanel = new JPanel();
				buttonsPanel.setLayout(new FlowLayout());
				result.setLayout(new BorderLayout());
				result.add(scroll, BorderLayout.CENTER);
				buttonsPanel.add(update);
				buttonsPanel.add(checkAccounts);
				result.add(buttonsPanel, BorderLayout.SOUTH);

				m_frame.add(result, BorderLayout.CENTER);
				m_frame.repaint(); m_frame.revalidate();
			}
		});
		
		JButton back = new JButton("Back");
		back.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				goToMainMenu_Regular();
			}
		});
		
		m_panel.add(searchField);
		m_panel.add(searchButton);
		m_panel.add(back);
		
		m_frame.add(m_panel);
		m_frame.repaint(); m_frame.revalidate();
	}
	
	private void goToCheckAccounts(final Client c)
	{
		m_frame.getContentPane().removeAll();
		m_panel = new JPanel();
		m_panel.setLayout(new BorderLayout());
		
		final List<Account> list = ((RegularUser) user).getAccounts(c);
		
		String columnData[][] = new String[list.size()][5];
		for(int i = 0; i < list.size(); ++i)
		{
			columnData[i][0] = Integer.toString(list.get(i).getId());
			columnData[i][1] = list.get(i).getType();
			columnData[i][2] = Long.toString(list.get(i).getBalance());
			columnData[i][3] = list.get(i).getCreationDate().toString();
			columnData[i][4] = Long.toString(list.get(i).getOwnerCnp());
		}
		
		String columnNames[] = new String[]{"Account ID", "Type", "Balance", "Creation Date", "Owner CNP"};
		final JTable table = new JTable(columnData, columnNames);
		JScrollPane scroll = new JScrollPane(table);
		
		JButton back = new JButton("Back");
		back.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				goToViewClientsPage();
			}
		});
		
		JButton transferMoney = new JButton("Transfer");
		transferMoney.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				int i = table.getSelectedRow();
				if(i != -1)
				{
					final Account acc = list.get(i);
					
					final JDialog dialog = new JDialog();
					dialog.setSize(200, 200);
					dialog.setLocationRelativeTo(null);
					dialog.setResizable(false);
					dialog.setTitle("Transfer");
					dialog.setModal(true);
					
					final JTextField sumField = new JTextField("Sum");
					final JTextField accField = new JTextField("AccountID");
					JButton done = new JButton("Done");
					done.addActionListener(new ActionListener(){
						public void actionPerformed(ActionEvent e) {
							try
							{
								long sumAmmount = Long.parseLong(sumField.getText());
								if(sumAmmount < 0) throw new Exception();
								((RegularUser) user).transferCash(acc,Integer.parseInt(accField.getText()), sumAmmount);
								
								dialog.dispose();
								goToCheckAccounts(c);
							}
							catch(Exception exc)
							{
								JDialog dialog = new JDialog();
								dialog.setTitle("Error");
								dialog.setModal(true);
								dialog.setSize(200,200);
								dialog.setResizable(false);
								dialog.setLocationRelativeTo(null);
								dialog.add(new JLabel("Invalid input!"));
								dialog.setVisible(true);
							}
						}
					});
					
					dialog.setLayout(new GridLayout(0,1));
					dialog.add(accField);
					dialog.add(sumField);
					dialog.add(done);
					
					dialog.setVisible(true);
				}
			}
		});
		
		JButton deleteAccount = new JButton("Delete");
		deleteAccount.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				int i = table.getSelectedRow();
				if(i != -1)
				{
					Account acc = list.get(i);
					((RegularUser) user).deleteAccount(acc);
					user.markAction("Account delete", "Deletion of account " + Integer.toString(acc.getId()));
					goToCheckAccounts(c);
				}
			}
		});
		
		JButton addAccount = new JButton("Add");
		addAccount.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {

				final JDialog dialog = new JDialog();
				dialog.setSize(300, 250);
				dialog.setLocationRelativeTo(null);
				dialog.setResizable(false);
				dialog.setTitle("Update Account");
				dialog.setModal(true);
					
				final JTextField typeField = new JTextField(20);
				final JTextField balanceField = new JTextField(20);
				final JTextField ownerCnpField = new JTextField(Long.toString(c.getCnp()));
				ownerCnpField.setEditable(false);
					
				JButton done = new JButton("Done");
				done.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent e) {
						String type = typeField.getText();
						long balance = Long.parseLong(balanceField.getText());
						
						((RegularUser) user).addAccount(c, type, balance);
						user.markAction("Account add", "Addition of account for " + c.getFirstName() + " " + c.getLastName());
						dialog.dispose();
						goToCheckAccounts(c);
					}
				});
					
				dialog.setLayout(new GridLayout(0,2));
				dialog.add(new JLabel("Type:")); dialog.add(typeField);
				dialog.add(new JLabel("Balance:")); dialog.add(balanceField);
				dialog.add(new JLabel("Owner CNP:")); dialog.add(ownerCnpField);
				dialog.add(done);
					
				dialog.setVisible(true);
			}
		});
		
		
		JButton updateAccount = new JButton("Update");
		updateAccount.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				int i = table.getSelectedRow();
				if(i != -1)
				{
					Account acc = list.get(i);

					final JDialog dialog = new JDialog();
					dialog.setSize(300, 250);
					dialog.setLocationRelativeTo(null);
					dialog.setResizable(false);
					dialog.setTitle("Update Account");
					dialog.setModal(true);
					
					final JTextField accountIdField = new JTextField(Integer.toString(acc.getId()));
					final JTextField typeField = new JTextField(acc.getType());
					final JTextField balanceField = new JTextField(Long.toString(acc.getBalance()));
					final JTextField ownerCnpField = new JTextField(Long.toString(acc.getOwnerCnp()));
					accountIdField.setEditable(false);
					ownerCnpField.setEditable(false);
					
					JButton done = new JButton("Done");
					done.addActionListener(new ActionListener(){
						public void actionPerformed(ActionEvent e) {
							int accountId = Integer.parseInt(accountIdField.getText());
							String type = typeField.getText();
							long balance = Long.parseLong(balanceField.getText());
							long ownerCnp = Long.parseLong(ownerCnpField.getText());
							
							((RegularUser) user).updateAccount(accountId, type, balance, ownerCnp);
							user.markAction("Account update", "Update of account " + Integer.toString(accountId));
							dialog.dispose();
							goToCheckAccounts(c);
						}
					});
					
					dialog.setLayout(new GridLayout(0,2));
					dialog.add(new JLabel("Account ID:")); dialog.add(accountIdField);
					dialog.add(new JLabel("Type:")); dialog.add(typeField);
					dialog.add(new JLabel("Balance:")); dialog.add(balanceField);
					dialog.add(new JLabel("Owner CNP:")); dialog.add(ownerCnpField);
					dialog.add(done);
					
					dialog.setVisible(true);
				}
			}
		});
		
		JPanel buttonsPanel = new JPanel();
		buttonsPanel.setLayout(new FlowLayout());
		buttonsPanel.add(transferMoney);
		buttonsPanel.add(addAccount);
		buttonsPanel.add(updateAccount);
		buttonsPanel.add(deleteAccount);
		buttonsPanel.add(back);
		
		m_panel.add(new JLabel(c.getFirstName() + " " + c.getLastName()), BorderLayout.NORTH);
		m_panel.add(scroll, BorderLayout.CENTER);
		m_panel.add(buttonsPanel, BorderLayout.SOUTH);
		
		m_frame.add(m_panel);
		m_frame.repaint(); m_frame.revalidate();
	}
}
